const pkg = require('./package')

module.exports = {
  mode: 'spa',

  /*
  ** Headers of the page
  */
  head: {
    title: "Brainstorm Cuneiform",
    script: [
      { src: 'https://cdn.jsdelivr.net/npm/@inkline/inkline/dist/inkline.js' },
      { src: '/__/firebase/7.15.5/firebase-app.js' },
      { src: 'https://firebase.google.com/docs/web/setup#available-libraries' },
      { src: '/__/firebase/init.js' }

    ],
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', type: 'text/css', href: 'https://fonts.googleapis.com/css?family=News+Cycle|Material+Icons' },
      { rel: 'stylesheet', type: 'text/css', href: 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css' },
      { rel: 'stylesheet', type: 'text/css', href: 'https://cdn.jsdelivr.net/npm/frow@3/dist/frow.min.css' },
      { rel: 'stylesheet', type: 'text/css', href: 'https://cdn.jsdelivr.net/npm/@inkline/inkline/dist/inkline.css' }
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#607d8b' },

  /*
  ** Global CSS
  */
  css: [
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    '@nuxtjs/style-resources',
    '@inkline/nuxt'
  ],
    inkline: {
            config: {
                variant: 'light'
            }   
        },
    styleResources: {
    scss: ['~/assets/brainstorm.scss']
  },
    // router: {
    //   base: '/brainshorm-cuneiform/',  
    // },
    generate: {
      dir: 'public',
    },

  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      
    }
  }
}
